{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE TemplateHaskell   #-}

module Main where

import           RIO hiding (on)
import           RIO.Process
import qualified RIO.Text                      as T

import qualified Data.Aeson                    as JSON
import qualified Data.Aeson.Types              as JSON
                                                ( typeMismatch )
import           Data.FileEmbed                 ( embedFile )
import           Data.Hashable                  ( Hashable )
import           GHC.Generics                   ( Generic )
import           GI.Gtk                         (Box (..), Label (..),
                                                 MenuBar (..), MenuItem (..),
                                                 Orientation (..))
import           GI.Gtk.Declarative
import           GI.Gtk.Declarative.App.Simple
import qualified System.Etc                    as Etc

--------------------------------------------------------------------------------

data Cmd
  = PrintConfig
  | RunMain
  deriving (Show, Eq, Generic)

instance Hashable Cmd

instance JSON.FromJSON Cmd where
  parseJSON json = case json of
    JSON.String cmdName
      | cmdName == "config" -> return PrintConfig
      | cmdName == "run"    -> return RunMain
      | otherwise -> JSON.typeMismatch ("Cmd (" <> T.unpack cmdName <> ")") json
    _ -> JSON.typeMismatch "Cmd" json

instance JSON.ToJSON Cmd where
  toJSON cmd = case cmd of
    PrintConfig -> JSON.String "config"
    RunMain     -> JSON.String "run"

--------------------------------------------------------------------------------

type MonadMyApp env m = (MonadReader env m, HasLogFunc env, HasProcessContext env, MonadIO m)

data MyApp = MyApp {
  logFunc        :: !LogFunc
, processContext :: !ProcessContext
}

instance HasLogFunc MyApp where
  logFuncL = lens logFunc (\x y -> x { logFunc = y })

instance HasProcessContext MyApp where
  processContextL = lens processContext (\x y -> x { processContext = y })

--------------------------------------------------------------------------------

configSpecData :: Text
configSpecData =
  T.decodeUtf8With lenientDecode $(embedFile "resources/spec.yaml")

newtype AppState = Message Text

data Event = Open | Save | Help
  
view' :: AppState -> Widget Event
view' (Message msg) =
  container Box [#orientation := OrientationVertical] $ do
    boxChild False False 0 $
      container MenuBar [] $ do
        subMenu "File" $ do
          menuItem MenuItem [on #activate Open] $ widget Label [#label := "Open"]
          menuItem MenuItem [on #activate Save] $ widget Label [#label := "Save"]
        menuItem MenuItem [on #activate Help] $ widget Label [#label := "Help"]
    boxChild True False 0 $
      widget Label [#label := msg]
 
update' :: AppState -> Event -> Transition AppState Event
update' _ = \case
  Open -> Transition (Message "Opening file...") (return Nothing)
  Save -> Transition (Message "Saving file...") (return Nothing)
  Help -> Transition (Message "There is no help.") (return Nothing)

main :: IO ()
main = do
  configSpec <- Etc.parseConfigSpec configSpecData

  Etc.reportEnvMisspellingWarnings configSpec

  (configFiles, _fileWarnings) <- Etc.resolveFiles configSpec
  (cmd        , configCli    ) <- Etc.resolveCommandCli configSpec
  configEnv                    <- Etc.resolveEnv configSpec

  let configDefault = Etc.resolveDefault configSpec

      config        = configDefault <> configFiles <> configEnv <> configCli

  logOptions     <- logOptionsHandle stdout True
  processContext <- mkDefaultProcessContext

  withLogFunc logOptions $ \logFunc -> case cmd of
    PrintConfig -> Etc.printPrettyConfig config

    RunMain     -> runRIO (MyApp logFunc processContext) $ do
      logInfo "Logging..."
      (w :: Int32) <- Etc.getConfigValue ["display", "width"]  config
      (h :: Int32) <- Etc.getConfigValue ["display", "height"] config
      liftIO $ run
        "Example GTK Declarative App"
        (Just (w, h))
         App
         { view = view'
         , update = update'
         , inputs = []
         , initialState = Message "Foodobee"
}
