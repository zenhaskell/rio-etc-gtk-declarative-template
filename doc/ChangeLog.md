# Changelog for shakebook

## v0.0.1.0

* Initial draft of shakebook.
* Allows for extensible pandoc-based markdown/latex book development.
* Comes pre-equipped with support for [inline-r](https://tweag.github.io/HaskellR/),
  [diagrams](https://archives.haskell.org/projects.haskell.org/diagrams/) and
  [dihaa](http://hackage.haskell.org/package/dihaa) drawings.
